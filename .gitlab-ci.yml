variables: &variables
  DEBIAN_FRONTEND: noninteractive
  DOCKER_HOST: tcp://docker:2375
  # Build parameters. We later override them at job level if needed.
  SONAR_SCANNER: "OFF"
  COVERALLS: "OFF"
  RUN_TESTS: "ON"
  STOCK_CPPZMQ: "ON"
  USE_PCH: "ON"
  CMAKE_BUILD_TYPE: "Debug"
  TANGO_USE_USING_NAMESPACE: "ON"
  BUILD_SHARED_LIBS: "ON"

services:
  - docker:20.10.1-dind

# See: https://docs.gitlab.com/ce/ci/yaml/README.html#workflowrules-templates
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

.job-template: &job-template
  image: debian:10
  before_script:
    - ulimit -c unlimited
    - echo "core.%e.%p.%t" > /proc/sys/kernel/core_pattern
    - apt-get update && apt-get install -y git wget unzip docker.io
    - .travis/before_install.sh
    - docker build -t cpp_tango .travis/${OS_TYPE}
    - >
        docker run
        --rm
        --name cpp_tango
        --user root
        -e BINTRAY_API_KEY=${CI_BINTRAY_API_KEY}
        -e BINTRAY_USER_NAME=tango-ci
        -e COVERALLS_REPO_TOKEN=${COVERALLS_REPO_TOKEN}
        -e SONAR_TOKEN=${SONAR_TOKEN}
        -e TANGO_HOST=${TANGO_HOST}
        -e TRAVIS_BRANCH=${TRAVIS_BRANCH}
        -e TRAVIS_JOB_ID=${TRAVIS_JOB_ID}
        -e DOCKER_HOST=tcp://$(getent hosts docker | awk '{ print $1 }'):2375
        -v `pwd`:/home/tango/src
        -v `pwd`/.sonar:/home/tango/.sonar
        -v `pwd`/idl:/home/tango/idl
        -v `pwd`/cppzmq:/home/tango/cppzmq
        -v `pwd`/tango_admin:/home/tango/tango_admin
        -v `pwd`/coveralls-cmake:/home/tango/coveralls-cmake
        -v `pwd`/build-wrapper-linux-x86:/home/tango/build-wrapper-linux-x86
        -v `pwd`/sonar-scanner:/home/tango/sonar-scanner
        -dit
        cpp_tango
    - .travis/install_tango_idl.sh
    - test ${STOCK_CPPZMQ} = "OFF" && .travis/install_cppzmq.sh || true
  script:
    - set -e
    - .travis/run.sh
    - .travis/install_tango.sh
    - .travis/install_tango_admin.sh
    - .travis/test.sh
    - set +e
    - test ${SONAR_SCANNER} = "ON" && .travis/sonar.sh || true
  after_script:
    - docker stop cpp_tango
    - find build -name 'core.*' -exec gzip "{}" \;
    - mkdir -p build/cpp_test_suite/test_results
    - gzip -r build/cpp_test_suite/test_results
  artifacts:
    when: always
    paths:
      - build/cpp_test_suite/test_results
      - build/cpp_test_suite/*/core.*

abi-api-compliance-check:
  variables:
    <<: *variables
    RUN_TESTS: "OFF"
  rules:
    # This job runs only for merge requests.
    - if: $CI_MERGE_REQUEST_IID
  image: debian:10
  before_script:
    - apt-get update && apt-get install -y git docker.io
    - git fetch origin "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
    - .travis/before_install.sh
    - docker build -t cpp_tango .travis/gcc-latest
    - >
        docker run
        --rm
        --name cpp_tango
        --user root
        -v `pwd`:/home/tango/src
        -v `pwd`/idl:/home/tango/idl
        -dit
        cpp_tango
    - .travis/install_tango_idl.sh
  script:
    - >
        docker exec
        -w /home/tango/src
        -e CI_TARGET_BRANCH=${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}
        -e CMAKE_BUILD_PARALLEL_LEVEL=$(nproc)
        cpp_tango
        .travis/check-ABI-API-compliance.sh || true
  after_script:
    - docker stop cpp_tango
  artifacts:
    when: always
    paths:
      - compat_reports

llvm-latest:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: llvm-latest
    RUN_TESTS: "OFF"
    WARNINGS_AS_ERRORS: "ON"
    TANGO_USE_USING_NAMESPACE: "OFF"
    USE_PCH: "OFF"

gcc-latest:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: gcc-latest
    RUN_TESTS: "OFF"
    WARNINGS_AS_ERRORS: "ON"
    TANGO_USE_USING_NAMESPACE: "OFF"
    USE_PCH: "OFF"

ubuntu-20.04:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: ubuntu-20.04

debian10:
  <<: *job-template
  variables:
    <<: *variables
    TANGO_ENABLE_COVERAGE: "ON"
    OS_TYPE: debian10
  after_script:
    - find build -name 'core.*' -exec gzip "{}" \;
    - mkdir -p build/cpp_test_suite/test_results
    - gzip -r build/cpp_test_suite/test_results
    - mkdir coverage
    - >
        docker exec --workdir /home/tango/src cpp_tango
        gcovr --filter '^cppapi/' --filter '^log4tango/(?!tests/)' -j$(nproc)
        --xml --output coverage.xml
    - >
        docker exec --workdir /home/tango/src cpp_tango
        gcovr --filter '^cppapi/' --filter '^log4tango/(?!tests/)' -j$(nproc)
        --html-details --output coverage/coverage.html
    - >
        docker exec --workdir /home/tango/src cpp_tango
        gcovr --filter '^cppapi/' --filter '^log4tango/(?!tests/)' -j$(nproc)
    - tar czf coverage.tar.gz coverage
    - docker stop cpp_tango
  artifacts:
    when: always
    reports:
      cobertura: coverage.xml
    paths:
      - build/cpp_test_suite/test_results
      - build/cpp_test_suite/*/core.*.gz
      - coverage.xml
      - coverage.tar.gz

debian10-static:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: debian10
    RUN_TESTS: "OFF"
    BUILD_SHARED_LIBS: "OFF"

debian10-no-pch:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: debian10
    USE_PCH: "OFF"
    RUN_TESTS: "OFF"

debian10-release:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: debian10
    CMAKE_BUILD_TYPE: Release

debian9:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: debian9

debian8:
  <<: *job-template
  variables:
    <<: *variables
    OS_TYPE: debian8
    # SONAR_SCANNER: "ON"
    # COVERALLS: "ON"
    STOCK_CPPZMQ: "OFF"

clang-analyzer:
  image: ubuntu:focal
  variables:
    DEBIAN_FRONTEND: noninteractive
    CC: /usr/lib/llvm-12/bin/clang
    CXX: /usr/lib/llvm-12/bin/clang++
  services: []
  before_script:
    - apt-get update
    - >
      apt-get install -y
      cmake
      curl
      gnupg
      omniidl
      libcos4-dev
      libomniorb4-dev
      libomnithread4-dev
      libzmq3-dev
    - curl https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -
    - >
      echo "deb http://apt.llvm.org/focal/ llvm-toolchain-focal-12 main"
      > /etc/apt/sources.list.d/llvm-toolchain-12.list
    - apt-get update
    - apt-get install -y clang-tools-12
    # Install tango-idl.
    - mkdir /idl
    - >
      curl https://gitlab.com/tango-controls/tango-idl/-/archive/main/tango-idl-main.tar.gz
      | tar --strip-components=1 -C /idl -zxf -
    - cmake -B/idl/build /idl
    - make -C /idl/build install
    # Create build directory for cppTango.
    - mkdir build
    # scan-build expects 'clang' binary to be available on PATH.
    - ln -s /usr/bin/clang-12 /usr/local/bin/clang
    - ln -s /usr/bin/clang-extdef-mapping-12 /usr/local/bin/clang-extdef-mapping
  script:
    - >
      cmake . -B build
      -DCMAKE_BUILD_TYPE=Debug
      -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
      -DBUILD_TESTING=OFF
      -DUSE_PCH=OFF
      -DTANGO_USE_USING_NAMESPACE=OFF
    # scan-build command must be wrapped in quotes because
    # --analyzer-config option cannot contain whitespaces.
    - "/usr/share/clang/scan-build-py-12/bin/analyze-build \
      -v \
      --cdb build/compile_commands.json \
      --exclude build/cppapi/server/idl \
      --keep-empty \
      --output clang-analyzer-results \
      --force-analyze-debug-code \
      --analyzer-config \
        stable-report-filename=true,\
        aggressive-binary-operation-simplification=true \
      --enable-checker core \
      --enable-checker cplusplus \
      --enable-checker deadcode \
      --enable-checker nullability \
      --enable-checker optin.cplusplus \
      --enable-checker optin.performance \
      --enable-checker optin.portability \
      --enable-checker security \
      --enable-checker unix \
      --enable-checker alpha.clone \
      --enable-checker alpha.core \
      --enable-checker alpha.cplusplus \
      --enable-checker alpha.deadcode \
      --enable-checker alpha.nondeterminism \
      --enable-checker alpha.unix \
      > clang-analyzer-output.txt"
  artifacts:
    when: always
    paths:
      - clang-analyzer-output.txt
      - clang-analyzer-results

clang-tidy:
  image: ubuntu:focal
  variables:
    DEBIAN_FRONTEND: noninteractive
    CC: /usr/lib/llvm-11/bin/clang
    CXX: /usr/lib/llvm-11/bin/clang++
  services: []
  before_script:
    - apt-get update
    - >
      apt-get install -y
      clang-tidy-11
      cmake
      git
      omniidl
      python3-minimal
      ruby-dev
      libcos4-dev
      libomniorb4-dev
      libomnithread4-dev
      libzmq3-dev
    # Install tango-idl.
    - git clone --depth 1 https://gitlab.com/tango-controls/tango-idl.git /idl
    - cmake -B/idl/build /idl
    - make -C /idl/build install
    # Install codeclimate. This is needed for generating HTML report.
    - git clone -b v0.85.22 --depth 1 https://github.com/codeclimate/codeclimate.git /codeclimate
    - gem build -C /codeclimate /codeclimate/codeclimate.gemspec
    - gem install /codeclimate/codeclimate-*.gem
    # Create build directory for cppTango.
    - mkdir build
  script:
    - >
      cmake . -B build
      -DCMAKE_BUILD_TYPE=Debug
      -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
      -DCMAKE_UNITY_BUILD=ON
      -DCMAKE_UNITY_BUILD_BATCH_SIZE=0
      -DBUILD_TESTING=OFF
      -DUSE_PCH=OFF
      -DTANGO_USE_USING_NAMESPACE=OFF
    - >
      run-clang-tidy-11
      -p build -header-filter='.*' 'cppapi/(?!server/idl)' 'log4tango/src'
      > clang-tidy-output.txt
    # Print warning summary (check name and occurrence count).
    - >
      grep -E 'warning: .+ \[.+\]$' clang-tidy-output.txt
      | sort | uniq | sed -E 's|^.+ \[(.+)\]|\1|' | sort | uniq -c
    # Produce a report in Code Climate JSON format.
    - >
      cat clang-tidy-output.txt
      | ./ci/clang-tidy-to-code-climate.py "$(pwd)/"
      > code-quality-report.json
    # Produce a report in HTML format.
    - >
      cat code-quality-report.json
      | ruby -I/codeclimate/lib ci/code-climate-json-to-html.rb "$(pwd)"
      > code-quality-report.html
  artifacts:
    when: always
    reports:
       codequality: code-quality-report.json
    paths:
      - clang-tidy-output.txt
      - code-quality-report.json
      - code-quality-report.html

cmake-with-locations:
  image: debian:buster
  variables:
    DEBIAN_FRONTEND: noninteractive
  services: []
  before_script:
    - apt-get update
    - >
      apt-get install -y
      build-essential
      cmake
      curl
      git
      pkg-config
      python3-dev
      python3-minimal
    # Install tango-idl
    - git clone --depth 1 https://gitlab.com/tango-controls/tango-idl.git /idl
    - cmake -B /idl/build -DCMAKE_INSTALL_PREFIX=/usr/local/tango-idl /idl
    - make -C /idl/build install
    # Install libzmq
    - git clone -b v4.0.5 --depth 1 https://github.com/zeromq/zeromq4-x.git /zmq
    - cmake -B /zmq/build -DCMAKE_INSTALL_PREFIX=/usr/local/zmq /zmq
    - make -C /zmq/build install
    # Install cppzmq
    - git clone -b v4.2.3 --depth 1 https://github.com/zeromq/cppzmq.git /cppzmq
    - export PKG_CONFIG_PATH=/usr/local/zmq/lib/pkgconfig
    - cmake -B /cppzmq/build -DCMAKE_INSTALL_PREFIX=/usr/local/cppzmq /cppzmq
    - make -C /cppzmq/build install
    # Install omniORB
    - curl -L https://sourceforge.net/projects/omniorb/files/omniORB/omniORB-4.2.1/omniORB-4.2.1.tar.bz2/download -o /omniORB.tar.bz2
    - mkdir /omniORB
    - tar xaf /omniORB.tar.bz2 --strip-components=1 -C /omniORB
    - cd /omniORB
    - ./configure --prefix=/usr/local/omniORB
    - make
    - make install
    - cd $CI_PROJECT_DIR
    - mkdir build
  script:
    - >
      cmake . -B build
      -DCMAKE_BUILD_TYPE=Debug
      -DBUILD_TESTING=ON
      -DCPPZMQ_BASE=/usr/local/cppzmq
      -DIDL_BASE=/usr/local/tango-idl
      -DOMNI_BASE=/usr/local/omniORB
      -DZMQ_BASE=/usr/local/zmq
    - make -C build
